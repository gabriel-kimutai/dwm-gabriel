
## My own personal build of dwm  **2023** 

![screenshot](./assets/dwm-git.png)
## Monochrome
![Monochrome](./assets/dwm-git-monochrome.png)
## Rose-pine?
![Rose-pine](./assets/dwm-rose-pine.png)
###  *Patches Included*
- [urgent-border](https://dwm.suckless.org/patches/urgentborder/)
- [autostart](https://dwm.suckless.org/patches/autostart/)
- [bar height](https://dwm.suckless.org/patches/bar_height/)
- [fibonacci-layout](https://dwm.suckless.org/patches/fibonacci/)
- [floatrules](https://dwm.suckless.org/patches/floatrules/)
- [fullgaps](https://dwm.suckless.org/patches/fullgaps/)
- [pertag](https://dwm.suckless.org/patches/pertag/)
- [self restart](https://dwm.suckless.org/patches/selfrestart/)
- [scratchpads](https://dwm.suckless.org/patches/scratchpads/)
- [swallow](https://dwm.suckless.org/patches/swallow/)
- [systray](https://dwm.suckless.org/patches/systray/)
- [xrdb](https://dwm.suckless.org/patches/xrdb/)


